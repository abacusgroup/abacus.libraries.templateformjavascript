﻿// this determines submission behavior
class Form {
    constructor(_excelFileName, _tickets, _additionProducts, _wizardAppName, _submissionName,
        _claimType, _newClaims, _entitlementName, _entitlementOptionName, _whatComesNext) {
        this.excelFileName = _excelFileName;
        this.tickets = _tickets ? _tickets : [];
        this.additionProducts = _additionProducts ? _additionProducts : [];
        this.wizardAppName = _wizardAppName;
        this.baseUrl = "https://uat.accessabacus.com/request/wizardgenerator/ajax";
        this.submissionName = _submissionName;
        this.claimType = _claimType;
        this.newClaims = _newClaims ? _newClaims : [];
        this.entitlementName = _entitlementName;
        this.entitlementOptionName = _entitlementOptionName;
        this.whatComesNext = _whatComesNext;

    }

    submit() {
        
        $("#next-btn").prop('disabled', true);
        $.post({
            url: this.baseUrl + "/submit",
            data: JSON.stringify(this),
            contentType: "application/json; charset=utf-8"
        })
            .then((response) => {
                $("#globalprotect-main").html(response);
            }, (error) => {
                console.log(error)
            })
    }
}

// this class affects the UI elements of the form
class Wizard {
    constructor(_rootUrl, _wizardTitle, _numberOfPages, _cardHeigth, _pages, _currentPageIndex, _clientAppMainDivName, _clientAppLoadingDivName) {
        this.rootUrl = _rootUrl
        this.wizardTitle = _wizardTitle;
        this.numberPages = _numberOfPages != "" ? _numberOfPages : "0";
        this.cardHeight = _cardHeigth ? _cardHeigth : "";
        this.pages = _pages ? _pages : [];
        this.currentPageIndex = _currentPageIndex != "" ? _currentPageIndex : 0;
        this.baseUrl = "https://uat.accessabacus.com/request/wizardgenerator/ajax";
        this.clientAppMainDivName = _clientAppMainDivName;
        this.clientAppLoadingDivName = _clientAppLoadingDivName;
    }

    // returns skeleton of page
    buildSkeleton() {
        $(`#${this.clientAppLoadingDivName}`).AjaxLoading("", true);
        $.get({
            url: this.baseUrl + `/GetSkeleton?wizardTitle=${this.wizardTitle}&numberOfPages=${this.numberPages}&cardHeight=${this.cardHeight}`,
        })
            .then((response) => {
                $(`#${this.clientAppLoadingDivName}`).AjaxLoading("", false);
                $(`#${this.clientAppMainDivName}`).html(response);
                this.loadInitialPage()
            }, (error) => {
                $(`#${this.clientAppLoadingDivName}`).AjaxLoading("", false);
                    console.log(error);
            })
    }

    loadInitialPage() {
        $("#skeleton-main").html(localStorage.getItem(this.pages[0].name));
        $("#main").html(localStorage.getItem(this.pages[0].componentName));
    }

    loadComponents() {
        for (var i = 0; i < this.pages.length; i++) {
            if (!localStorage.getItem(this.pages[i].componentName)) {
                $.get({
                    async: false,
                    url: rootUrl + `ajax/LoadComponent?componentName=${this.pages[i].componentName}`
                })
                    .then((response) => {
                        localStorage.setItem(this.pages[i].componentName, response);
                    })
            }
        }
    }

    loadPages() {
        for (var i = 0; i < this.pages.length; i++) {
            if (this.pages[i].name !== this.pages[this.pages.length - 1].name) {
                if (!localStorage.getItem(this.pages[i].name)) {
                    $.get({
                        async: false,
                        url: this.baseUrl + `/GetCustomPage?cardHeader=${this.pages[i].header}&cardSubHeader=${this.pages[i].subHeader}&cardSubheader2=${this.pages[i].subheader2}`
                    })
                        .then((response) => {
                            localStorage.setItem(this.pages[i].name, response);
                        })
                }
            }
        }
    }

    loadConfirmation() {
        $("#loading-div").AjaxLoading("", true);
        console.log(JSON.stringify(this.pages[this.pages.length - 1].purchasedItems))
        $.post({
            url: this.baseUrl + "/GetConfirmationPage",
            data: JSON.stringify(this.pages[this.pages.length - 1].purchasedItems),
            contentType: "application/json; charset=utf-8",
            dataType: "html"
        })
            .then((response) => {
                $("#skeleton-main").html(response);
                $("#loading-div").AjaxLoading("", false);
                setNextButtonText("Complete Purchase");
            }, (error) => {
                $("#loading-div").AjaxLoading("", false);
                loadError();
            })
    }

    next() {
        if (this.currentPageIndex == this.pages.length - 1)
            submit();
        else if (wizard.currentPageIndex == wizard.pages.length - 2) {

            setBackButtonText("Back");
            this.loadConfirmation();
            wizard.currentPageIndex += 1;
            setPageNumber(wizard.currentPageIndex + 1);
        }
        else {
            var nextPage = wizard.pages[wizard.currentPageIndex + 1];
            if (nextPage.name == "globalprotect-confirmation")
                setNextButtonText("Complete Purchase");
            if (wizard.currentPageIndex == 0)
                setBackButtonText("Back");
            $("#skeleton-main").html(localStorage.getItem(nextPage.name));
            $("#main").html(localStorage.getItem(nextPage.componentName));
            wizard.currentPageIndex += 1;
            // set current page number
            setPageNumber(wizard.currentPageIndex + 1);

        }
    }

    back() {
        if (this.currentPageIndex == 0)
            window.location.href = "/../apps/addons";
        else {
            if (this.currentPageIndex == this.pages.length - 1) {
                setNextButtonText("Next");
                $("#next-btn").removeClass('btn-secondary');
                $("#next-btn").addClass('btn-primary');
            }
            if (this.currentPageIndex == 1) {
                setBackButtonText("Cancel");
                disableNext();
            }
        }

        var previousPage = this.pages[this.currentPageIndex - 1];
        $("#skeleton-main").html(localStorage.getItem(previousPage.name));
        $("#main").html(localStorage.getItem(previousPage.componentName));
        wizard.currentPageIndex -= 1;
        setPageNumber(this.currentPageIndex + 1);
    }
}